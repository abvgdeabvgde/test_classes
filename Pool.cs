﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

public class Pool
{
    public GameObject[] prefabs;

    private List<GameObject> active;
    private Queue<GameObject> deactivated;
    private Transform holder;

    public Pool(GameObject[] prefabs, int initialSize)
    {
        this.prefabs = prefabs;
        Array.ForEach(prefabs, x => x.SetActive(false));

        string poolName = "POOL: ";
        Array.ForEach(prefabs, x => poolName += x.name + " & ");
        holder = new GameObject(poolName).transform;

        deactivated = new Queue<GameObject>(initialSize * prefabs.Length);
        active = new List<GameObject>(initialSize * prefabs.Length);

        for (int i = 0; i < initialSize; i++)
        {
            for (int j = 0; j < prefabs.Length; j++)
            {
                var go = Add(j);

                deactivated.Enqueue(go);
            }
        }
    }

    internal void Reset()
    {
        active.ForEach(x => Return(x));
    }

    private GameObject Add(int i = -1)
    {
        if (i == -1)
        {
            i = UnityEngine.Random.Range(0, prefabs.Length);
        }
        GameObject go = GameObject.Instantiate(prefabs[i], holder);
        IPoolable poolable = go.GetComponent<IPoolable>();
        if (poolable != null)
        {
            poolable.Pool = this;
        }
        return go;
    }

    public GameObject Get()
    {
        if (deactivated.Count == 0)
        {
            deactivated.Enqueue(Add());
        }

        active.Add(deactivated.Peek());
        return deactivated.Dequeue();
    }

    public bool Return(GameObject obj)
    {
        obj.SetActive(false);
        deactivated.Enqueue(obj);
        return active.Remove(obj);
    }

    public string DebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(active.Count);
        sb.Append("--- Active ---\n");
        foreach (var item in active)
        {
            sb.Append("<color=");
            sb.Append(item.activeSelf ? "green>" : "red>");
            sb.Append(item.name);
            sb.Append("</color>\n");
        }
        sb.Append(deactivated.Count);
        sb.Append("--- Deactivated ---\n");
        foreach (var item in deactivated)
        {
            sb.Append("<color=");
            sb.Append(item.activeSelf ? "green>" : "red>");
            sb.Append(item.name);
            sb.Append("</color>\n");
        }

        return sb.ToString();
    }
}

public interface IPoolable
{
    Pool Pool { get; set; }
}