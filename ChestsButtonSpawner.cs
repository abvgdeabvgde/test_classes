﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(LayoutGroup))]
public class ChestsButtonSpawner : MonoBehaviour
{
    public GameObject prefab;
    public GameObject collectButton;

    private const float SKIN_CHANCE = 0.12f;

    private static int[] coinsPrizes = new int[9] {
        10, 25, 25,
        25, 25, 50,
        50, 50, 100 };

    private int keysPerFrame;

    private ChestButtonScript[] buttons;

    private void Awake()
    {
        buttons = new ChestButtonScript[coinsPrizes.Length];
        for (int i = 0; i < coinsPrizes.Length; i++)
            buttons[i] = Instantiate(prefab, transform).GetComponent<ChestButtonScript>();
    }

    private void OnEnable()
    {
        ResetButtons();
    }

    private void ResetButtons()
    {
        keysPerFrame = 3;

        collectButton.SetActive(false);

        int skinId = Random.value < SKIN_CHANCE ? GetRandomLockedSkinId() : -1;
        int jackpotId = skinId == -1 ? -1 : Random.Range(0, 9);

        coinsPrizes.Shuffle();
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].Reset();

            buttons[i].parent = this;

            if (i == jackpotId)
            {
                buttons[i].type = ChestButtonScript.PrizeType.Skin;
                buttons[i].number = skinId;
                buttons[i].skin.purchased = true;
                buttons[i].skin.UpdateColors();
            }
            else
            {
                buttons[i].type = ChestButtonScript.PrizeType.Coins;
                buttons[i].number = coinsPrizes[i];
            }
        }

    }

    public bool IsClickAllowed()
    {
        if (SaveManager.Instance.Keys <= 0) return false;

        SaveManager.Instance.Keys--;
        keysPerFrame--;
        if (SaveManager.Instance.Keys <= 0 || keysPerFrame == 0) collectButton.SetActive(true);

        return true;
    }

    private int GetRandomLockedSkinId()
    {
        if (SaveManager.Instance.LockedSkinCount == 0) return -1;

        int id = -1;
        while (id < 0)
        {
            id = Random.Range(0, SaveManager.Instance.skinsUnlocked.Length);
            if (SaveManager.Instance.skinsUnlocked[id]) id = -1;
        }
        return id;
    }
}
