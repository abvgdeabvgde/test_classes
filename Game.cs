﻿using System;
using System.Collections;
using UnityEngine;

public class Game : MonoBehaviour
{
    public static Game instance;

    public TMPro.TextMeshProUGUI scoreText;
    public CanvasGroup gameOverCanvasGroup;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        Application.targetFrameRate = Consts.TargetFrameRate;
        QualitySettings.vSyncCount = Consts.VSyncCount;
    }

    private IEnumerator Start()
    {
        Play();

        Time.timeScale = 50;
        for (int i = 0; i < 5; i++)
        {
            yield return null;
        }
        Time.timeScale = 1;
    }

    private int _distance;

    public int Distance
    {
        get => _distance;
        set
        {
            _distance = value;
            scoreText.text = value.ToString();
            OnDistanceChange?.Invoke(this, EventArgs.Empty);
        }
    }

    public static event EventHandler OnDistanceChange;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Play();
        }
    }

    public void Play()
    {
        gameOverCanvasGroup.alpha = 0;
        Distance = 0;
    }

    public void GameOver()
    {
        StartCoroutine(GameOverCoroutine());

        IEnumerator GameOverCoroutine()
        {
            float current = 0;
            float target = Consts.GameOverAnimationDuration;
            float t = 0;
            while (current < target)
            {
                yield return null;

                current += Time.deltaTime;
                t = current / target;

                gameOverCanvasGroup.alpha = t;
            }
        }
    }

}
