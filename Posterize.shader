﻿Shader "Hidden/Posterize"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Colors("Colors Count", Range(1, 255)) = 255
		_Quality("Quality", Range(0.0, 8.0)) = 1.0
	}
		SubShader
		{
			// No culling or depth
			Cull Off ZWrite Off ZTest Always

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				float _Colors;
				float _Quality;

				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, i.uv);
					float q = 2.0 + _Quality * 8.0; // (1-255)
					float3 q3 = q; //_Colors, _Colors, _Colors);
					col = fixed4(floor(col.rgb * q) / q, col.a);
					return col;
				}
				ENDCG
			}
		}
}
