﻿Shader "Hidden/SmokingTrail"
{
	Properties
	{
		_MainTex("Texture", 2D) = "" {}
		_PrevFrame("Previous Frame", 2D) = "" {}
		_Fade("Fade", Range(0.0, 1.0)) = 0.9
		_Gravity("Gravity", Range(-2, 2)) = 0.5
		_Turbulance("Turbulance", Float) = 20
		_BlurSize("Blur Size", Range(0, 0.02)) = 0.0001
		_BlurAmount("Blur Amount", Range(0.0, 1.0)) = 0.05
		_Samples("Samles", Range(1, 64)) = 1 //46
	}
		SubShader
		{
			//Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
			//Blend SrcAlpha OneMinusSrcAlpha
			Cull Off ZWrite Off ZTest Always

			Pass
			{

			//Color(0,0,0,0)

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					return o;
				}

				sampler2D _MainTex;
				sampler2D _PrevFrame;
				float2 _MainTex_TexelSize;
				float _Fade;
				float _Gravity;
				float _Turbulance;
				float _BlurSize;
				float _BlurAmount;
				float _Samples;

				float hash(float n)
				{
					return frac(sin(n) * 43758.5453);
				}

				float noise(float3 x)
				{
					float3 p = floor(x);
					float3 f = frac(x);

					f = f * f * (3.0 - 2.0 * f);
					float n = p.x + p.y * 57.0 + 113.0 * p.z;

					return lerp(lerp(lerp(hash(n + 0.0), hash(n + 1.0), f.x),
						lerp(hash(n + 57.0), hash(n + 58.0), f.x), f.y),
						lerp(lerp(hash(n + 113.0), hash(n + 114.0), f.x),
							lerp(hash(n + 170.0), hash(n + 171.0), f.x), f.y), f.z);
				}


				float normpdf(float x, float sigma)
				{
					return 0.39894 * exp(-0.5 * x * x / (sigma * sigma)) / sigma;
				}

				fixed4 blur(sampler2D tex, float2 uv) {
					fixed4 color = tex2D(tex, uv);
					const int mSize = _Samples;
					const int iter = (mSize - 1) / 2;
					for (int i = -iter; i <= iter; ++i) {
						for (int j = -iter; j <= iter; ++j) {
							color += tex2D(tex, float2(uv.x + i * _BlurSize, uv.y + j * _BlurSize)) * normpdf(float(i), 7);
						}
					}
					return color / mSize;
				}

				fixed4 blur2(float2 uv) {
					/*fixed4 col = 0;
					float2 dist = uv - float2(_CenterX, _CenterY);
					for (int j = 0; j < _Samples; j++) {
						float scale = 1 - _EffectAmount * (j / _Samples) * (saturate(length(dist) / _Radius));
						col += tex2D(_MainTex, dist * scale + float2(_CenterX, _CenterY));
					}
					col /= _Samples;
					return col;*/
			}


		   fixed4 frag(v2f i) : SV_Target
		   {
			   float noiseValue = noise(_Turbulance * float3(i.uv.xy, i.uv.x) + _Time) - 0.5;
			   float2 uv = i.uv + float2(noiseValue * unity_DeltaTime.x, _Gravity * unity_DeltaTime.x);

			   fixed4 cur = tex2D(_MainTex, i.uv);
			   fixed4 old = tex2D(_PrevFrame, uv);
			   old = lerp(old, blur(_PrevFrame, i.uv), _BlurAmount);

			   //if (old.a > 0)
			   old = lerp(cur, old, _Fade);
			   return lerp(old, cur, cur.a);
		   }

		   ENDCG
	}
		}
}
