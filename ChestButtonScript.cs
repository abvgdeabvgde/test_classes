﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestButtonScript : MonoBehaviour
{
    public enum PrizeType { Coins, Skin };

    [HideInInspector] public ChestsButtonSpawner parent;

    public GameObject chest;
    public SkinButtonScript skin;
    public TMPro.TextMeshProUGUI coinsText;


    [HideInInspector] public PrizeType type;
    [HideInInspector] public int number;

    public void OnClickAction()
    {
        if (parent.IsClickAllowed())
        {
            chest.SetActive(false);
            switch (type)
            {
                case PrizeType.Coins:
                    {
                        coinsText.gameObject.SetActive(true);
                        coinsText.text = "<sprite=1> " + number.ToString();
                        SaveManager.Instance.Coins += number;
                        break;
                    }
                case PrizeType.Skin:
                    {
                        skin.gameObject.SetActive(true);
                        SaveManager.Instance.UnlockSkin(number);
                        SaveManager.Instance.SkinSelected = number;
                        break;
                    }
            }
            AudioManager.instance.Play(AudioManager.AudioClips.Chest);
        }
    }

    internal void Reset()
    {
        chest.SetActive(true);
        skin.gameObject.SetActive(false);
        coinsText.gameObject.SetActive(false);
        coinsText.text = "";
    }
}
